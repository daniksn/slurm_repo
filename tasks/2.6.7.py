
if __name__ == '__main__':
    branch_name = str(input())
    test_pass_result = int(input())
    coverage_change = float(input())
    approve_count = int(input())

    if branch_name.lower() not in ("development","staging"):
        print(f"В ветке {branch_name} непроверенный код, пропускаем")
    elif (test_pass_result == 1 and coverage_change > 5) or (test_pass_result == 1 and coverage_change <= 5 and approve_count > 3):
        print(f"Внимание! Код из {branch_name} отправлен в релиз!")
    else:
        print(f"Код из {branch_name} с результатами тесты: {test_pass_result}, coverage: {coverage_change}, approve: {approve_count} в релиз не попадает.")